# coding: utf-8

import unittest
import finder


class BaseTestCase(unittest.TestCase):
    def assertNameCompoundFound(self, text):
        compounds = finder.find(text)
        self.assertEqual(1, len(compounds))

    def assertMultipleNameCompounds(self, text, expected):
        compounds = set([c.words_as_string() for c in finder.find(text)])
        self.assertEqual(compounds, expected)


class TestBareNounCompounds(BaseTestCase):
    def testDishNames(self):
        self.assertNameCompoundFound('şiş kebap')
        self.assertNameCompoundFound('kuzu fırın')

    def testGenders(self):
        self.assertNameCompoundFound('erkek kardeş')
        self.assertNameCompoundFound('kadın doktor')
        self.assertNameCompoundFound('avukat kadın')

    def testGenderOneWords(self):
        self.assertNameCompoundFound('kızarkadaş')

    def testNationality(self):
        self.assertNameCompoundFound('Alman mimar')
        self.assertNameCompoundFound('Türk çocuklar')

    def testMaterial(self):
        self.assertNameCompoundFound('çelik kapı')
        self.assertNameCompoundFound('taş duvar')
        self.assertNameCompoundFound('yün çorap')

    def testStreetNames(self):
        self.assertNameCompoundFound('Akgül sokak')
        self.assertNameCompoundFound('Ada Sokak')


class TestBareAdjectiveNounCompounds(BaseTestCase):
    def testOneWords(self):
        self.assertNameCompoundFound('karabiber')
        self.assertNameCompoundFound('karafatma')
        self.assertNameCompoundFound('darboğaz')
        self.assertNameCompoundFound('kabakulak')
        self.assertNameCompoundFound('akciğer')
        self.assertNameCompoundFound('sarı humma')


class TestICompounds(BaseTestCase):
    def testNounNounThirdPossive(self):
        self.assertNameCompoundFound('otobüs bileti')
        self.assertNameCompoundFound('el çantası')
        self.assertNameCompoundFound('masa örtüsü')
        self.assertNameCompoundFound('çay bardağı')
        self.assertNameCompoundFound('çocuk bahçesi')

    def testNounNounThirdPossivePlural(self):
        self.assertNameCompoundFound('otobüs biletleri')
        self.assertNameCompoundFound('el çantaları')
        self.assertNameCompoundFound('çay bardakları')

    def testNounOneWordPlural(self):
        self.assertNameCompoundFound('ayakkabılar')
        self.assertNameCompoundFound('yüzbasşılar')
        self.assertNameCompoundFound('binbaşılar')

    def testRatePluralPlural(self):
        self.assertNameCompoundFound('kasımpatları')
        self.assertNameCompoundFound('kasımpatılar')

    def testNameAndCategory(self):
        self.assertNameCompoundFound('Ağrı Dağı')
        self.assertNameCompoundFound('Van Gölü')
        self.assertNameCompoundFound('Efes Lokantası')
        self.assertNameCompoundFound('Türkiye Cumhuriyeti')

    def testDateTime(self):
        self.assertNameCompoundFound('1912 yılı')
        self.assertNameCompoundFound('cuma günü')
        self.assertNameCompoundFound('ocak ayı')

    def testNationality(self):
        self.assertNameCompoundFound('Türk kahvesi')
        self.assertNameCompoundFound('Türk Lirası')
        self.assertNameCompoundFound('Alman ekmeği')
        self.assertNameCompoundFound('Fransız edebiyatı')

    def testNationalityWithBareWord(self):
        self.assertNameCompoundFound('Yunan müziği')
        self.assertNameCompoundFound('Hint dilleri')

    def testNationalityWithLiSuffix(self):
        self.assertNameCompoundFound('Yunanlı doktor')
        self.assertNameCompoundFound('Hintli müzisyen')

    def testCountriesWithNoSeparateNationalities(self):
        self.assertNameCompoundFound('Çin feneri')
        self.assertNameCompoundFound('Brezilya takımı')

    def testGeographicalAndGeopoliticalEntities(self):
        self.assertNameCompoundFound('Afrika ülkesi')
        self.assertNameCompoundFound('Ortadoğu siyaseti')
        self.assertNameCompoundFound('Akdeniz iklimi')
        self.assertNameCompoundFound('Uşak halısı')

    def testNationalityAndPersonWithSuffix(self):
        self.assertNameCompoundFound('İngiliz çocukları')
        self.assertNameCompoundFound('Türk işçileri')
        self.assertNameCompoundFound('Japon askerleri')
        self.assertNameCompoundFound('bir Rus sporcusu')
        self.assertNameCompoundFound('Yunan başbakanı')


class TestComplexCompounds(BaseTestCase):
    def testThreeWords(self):
        self.assertMultipleNameCompounds('İngiliz Edebiyatı Bölümü',
            [
                'İngiliz Edebiyatı Bölümü',
                'İngiliz Edebiyatı'
            ])
        self.assertMultipleNameCompounds('göz hastalıkları hastanesi',
            [
                'göz hastalıkları hastanesi',
                'göz hastalıkları',
            ])

if __name__ == '__main__':
    unittest.main()
