The MIT License (MIT)

Copyright (c) <2013> <Haldun Bayhantopcu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

================================================================================

The Boun Morphological Parser License:

The Boun Morphological Parser is distributed under the Attribution-Noncommercial-Share Alike license.
A human-readable summary of the Legal Code is as follows:
Attribution — You must attribute the work in the manner specified by the author or licensor.
Noncommercial — You may not use this work for commercial purposes.
Share Alike — If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.
For the full license, see http://creativecommons.org/licenses/by-nc-sa/3.0/.

Attribution Info:
Please cite the following paper if you make use of this resource in your research.
Note that this paper describes a preliminary version of the parser, another paper explaining the stochastic parser has been submitted for review.

Haşim Sak, Tunga Güngör, and Murat Saraçlar. Turkish Language Resources: Morphological Parser, Morphological Disambiguator and Web Corpus. In GoTAL 2008, volume 5221 of LNCS, 2008, pages 417–427. Springer.

BibTeX entry:
@InProceedings{sak:08,
        Author = {Ha{\c s}im Sak and Tunga G{\"u}ng{\"o}r and Murat Sara{\c c}lar},
        Booktitle = {GoTAL 2008},
        Pages = {417--427},
        Title = {Turkish Language Resources: Morphological Parser, Morphological Disambiguator and Web Corpus},
        Volume = {5221},
        Series = {LNCS},
        Publisher = {Springer},
        Year = {2008}
}

Contact Info:
Haşim Sak
Department of Computer Engineering
Boğaziçi University
34342 Bebek, İstanbul, Turkey
hasim.sak@gmail.com
hasim.sak@boun.edu.tr

May 27, 2009
