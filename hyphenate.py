#coding: utf-8

tests = (
    ("cccvcc", 5),
    ("cccvcv", 4),
    ("cccvc", 5),
    ("ccvccc", 5),
    ("ccvccv", 4),
    ("ccvcc", 5),
    ("ccvcc", 4),
    ("ccvcv", 3),
    ("ccvc", 4),
    ("ccvv", 3),
    ("ccv", 3),
    ("cvccc", 4),
    ("cvccv", 3),
    ("cvcc", 4),
    ("cvcv", 2),
    ("cvc", 3),
    ("cv", 2),
    ("vccc", 3),
    ("vccv", 2),
    ("vcc", 3),
    ("vcvc", 1),
    ("vcv", 1),
    ("vc", 2),
    ("v", 1),
)

vowels = ('a', 'A', 'e', 'E', 'ı', 'I', 'i', 'İ',
          'o', 'O', 'ö', 'Ö', 'u', 'U', u'ü', u'Ü')


def is_vowel(char):
    return char in vowels


def convert(word):
    return u''.join([is_vowel(char) and 'v' or 'c' for char in word])


def hypenate(word):
    cv_form = convert(word)
    syllables = []

    while cv_form:
        print cv_form, word
        for form, length in tests:
            if cv_form.startswith(form):
                syllables.append(word[:length])
                cv_form = cv_form[length:]
                word = word[length:]
                break
        return []

    return syllables


def main():
    words = ['trabzon', 'türk', 'kramp', 'iyi',
             'çekoslavakyalılaştıramadıklarımızdanmısınız',
             'haldun', 'slovakya']
    for word in words:
        print word, hypenate(word)

if __name__ == '__main__':
    main()

