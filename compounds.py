#coding: utf-8

import StringIO

import syllables
from morphology import Word
from sentences import parse_sentence

# NounCompound = <Modifier, Head>
# where Modifier is a NounCompound and Head is a NounCompound
#       Modifier is a NounCompound and Head is P3sg or P3pl
#       Modifier is a Noun Phrase  and Head is P3sg or P3pl
#       Modifier is a Bare Noun    and Head is P3sg or P3pl
#       Modifier is a Bare Noun    and Head is a NounCompound
#       Modifier is a Noun + Adj*  and Head is P3sg or P3pl

# Simple NounCompounds:
#       Modifier is a Noun Phrase  and Head is P3sg or P3pl
#       Modifier is a Bare Noun    and Head is P3sg or P3pl
#       Modifier is a Noun + Adj*  and Head is P3sg or P3pl

# Complex NounCompounds:
#       Modifier is a NounCompound and Head is a NounCompound
#       Modifier is a NounCompound and Head is P3sg or P3pl
#       Modifier is a Bare Noun    and Head is a NounCompound

class NounCompound(object):
    def __init__(self, modifier=None, head=None):
        self.head = head
        self.modifier = modifier

    def __str__(self):
        return "(NC %s, %s)" % (self.modifier, self.head)


class AdjectiveNounPhrase(object):
    def __init__(self, adjective=None, noun=None):
        self.adjective = adjective
        self.noun = noun

    def __str__(self):
        return "(NP %s, %s)" % (self.adjective, self.noun)


def is_noun_phrase(x):
    return isinstance(x, AdjectiveNounPhrase)


def is_word(x):
    return isinstance(x, Word)


def is_noun_compound(x):
    return isinstance(x, NounCompound)


class SentenceIterator(object):
    def __init__(self, sentence):
        self.parsed_sentence = sentence
        self.index = 1

    def __iter__(self):
        return self

    def next(self):
        if self.index >= len(self.parsed_sentence):
            raise StopIteration

        prev, current = self.parsed_sentence[self.index - 1], self.parsed_sentence[self.index]
        next = None

        try:
            next = self.parsed_sentence[self.index + 1]
        except:
            pass

        self.index += 1
        return prev, current, next

    def skipnext(self):
        self.index += 1


def mark_noun_phrases(parsed_sentence):
    retval = []
    iterator = SentenceIterator(parsed_sentence)

    for prev, current, next in iterator:
        if prev.is_adjective and current.is_noun:
            retval.append(AdjectiveNounPhrase(prev, current))
            iterator.skipnext()
        else:
            retval.append(prev)

        # Do not miss the last word
        if next is None:
            retval.append(current)

    return retval


def mark_simple_compounds(parsed_sentence):
    retval = []
    iterator = SentenceIterator(parsed_sentence)

    for prev, current, next in iterator:
        if is_noun_phrase(prev) and is_word(current) and current.is_third_person_possive:
            retval.append(NounCompound(prev, current))
            iterator.skipnext()
        elif is_word(prev) and prev.is_bare and is_word(current) and current.is_third_person_possive:
            retval.append(NounCompound(prev, current))
            iterator.skipnext()
        else:
            retval.append(prev)

        if next is None:
            retval.append(current)

    return retval


# Complex NounCompounds:
#       Modifier is a NounCompound and Head is a NounCompound
#       Modifier is a NounCompound and Head is P3sg or P3pl
#       Modifier is a Bare Noun    and Head is a NounCompound
def mark_complex_compounds(parsed_sentence):
    retval = []
    iterator = SentenceIterator(parsed_sentence)

    for prev, current, next in iterator:
        if is_noun_compound(prev) and is_noun_compound(current):
            retval.append(NounCompound(prev, current))
            iterator.skipnext()
        elif is_noun_compound(prev) and is_word(current) and current.is_third_person_possive:
            retval.append(NounCompound(prev, current))
            iterator.skipnext()
        elif is_word(prev) and prev.is_bare and is_noun_compound(current):
            retval.append(NounCompound(prev, current))
            iterator.skipnext()
        else:
            retval.append(prev)

        if next is None:
            retval.append(current)

    return retval


def process_sentence(sentence):
    parsed_sentence = parse_sentence(sentence)
    return mark_complex_compounds(mark_simple_compounds(mark_noun_phrases(parsed_sentence)))


def sentence_to_sexp(processed_sentence):
    output = StringIO.StringIO()
    for element in processed_sentence:
        print >>output, element,
    return output.getvalue()


def main():
    sentence = 'Türkiye Cumhuriyeti Milli Eğitim Bakanlığı Kadıköy Kız Yüksek Meslek Okulu Müdürlüğü'
    parsed_sentence = parse_sentence(sentence)

    print sentence_to_sexp(process_sentence(sentence))

if __name__ == '__main__':
    main()

