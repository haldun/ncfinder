import re
from morphology import Word

def clean_text(text):
    if text is None:
        text = ''
    text = text.strip()
    return text


SENTENCE_ENDERS = re.compile('[.!?]')
def extract_sentences(cleaned_text):
    return [s.strip() for s in SENTENCE_ENDERS.split(cleaned_text) if s]


def parse_sentence(sentence):
    return [Word(word) for word in sentence.split(' ')]

