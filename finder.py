#coding: utf-8

import operator
import re

import TurkishMorphology
TurkishMorphology.load_lexicon('turkish.fst')

class Word(object):

    def __init__(self, original):
        self.original = original
        self.root = None
        self.attrs = []
        self.is_parsed = False
        self.attrs_set = set()
        self._parse(original)

    def __repr__(self):
        return "<Word: '%s', root: '%s', attrs: %s>" % (self.original, self.root, self.attrs)

    @property
    def has_suffix(self):
        return self.original != self.root

    @property
    def is_bare(self):
        return not self.has_suffix

    @property
    def is_profession(self):
        return self.root in set(['doktor', 'avukat'])

    @property
    def is_gender(self):
        return self.root in set(['kadın', 'erkek', 'kız'])

    @property
    def is_nationality(self):
        return self.root in set(['Türk', 'Alman'])

    @property
    def is_noun(self):
        return 'Noun' in self.attrs

    @property
    def is_adjective(self):
        return 'Adj' in self.attrs

    @property
    def is_third_person_possive(self):
        return bool(set(['P3sg', 'P3pl']) & self.attrs_set)

    @property
    def is_nominative(self):
        return 'Nom' in self.attrs_set

    @property
    def is_plural(self):
        for attr in self.attrs_set:
            if 'pl' in attr:
                return True
        return False

    @property
    def is_material(self):
        # TODO Look-up from a list of words
        return self.root in set(['çelik', 'taş', 'yün', 'Çelik'])

    @property
    def is_street(self):
        return self.root == 'sokak'

    def _parse(self, original):
        parses = sorted(TurkishMorphology.parse(original), key=operator.itemgetter(1))

        if not parses:
            return

        best_parse = parses[0][0]
        self.root = best_parse[:best_parse.find('[')]

        for match in re.findall("(?P<attr>\[\w+\])", best_parse):
            self.attrs.append(match[1:-1])

        self.attrs_set = set(self.attrs)
        self.is_parsed = True


class NounCompound(object):
    BARE_COMPOUND_NOUN = 1

    BARE_NOUN_COMPOUND_TYPE_1 = 1
    BARE_NOUN_COMPOUND_TYPE_2 = 2
    BARE_NOUN_COMPOUND_TYPE_3 = 3
    BARE_NOUN_COMPOUND_TYPE_4 = 4
    BARE_NOUN_COMPOUND_TYPE_5 = 5

    BARE_ADJ_NOUN_COMPOUND_TYPE = 6
    I_COMPOUND_TYPE = 7

    def __init__(self, words=None, type=None):
        self.words = words is None and [] or words
        self.type = type is None and None or type

    def append(self, word):
        self.words.append(word)

    def __repr__(self):
        return "<NounCompound: %s, %s>" % (self.words, self.type)

    def __str__(self):
        return self.words_as_string()

    def words_as_string(self):
        return " ".join([word.original for word in self.words])


def is_a_dish_name(words):
    # TODO look-up from a list of words
    w = " ".join(w.original for w in words)
    return w in set(['şiş kebap', 'kuzu fırın'])


"""

goz hastaliklari hastanesi

((goz hastaliklari) hastanesi)

goz hastaliklari
goz hastaliklari hastanesi


goz hastaliklari        => found
hastaliklari hastanesi  => not found. any immediate compound? if yes, look for if hastanesi is i_s?

"""

def find_noun_compounds(text):
    cleaned_text = clean_text(text)
    sentences = extract_sentences(cleaned_text)
    parsed_sentences = (parse_sentence(sentence) for sentence in sentences)
    all_compounds = []
    for sentence in parsed_sentences:
        compounds = SentenceScanner(sentence).scan()
        all_compounds.extend(compounds)
    return all_compounds


def clean_text(text):
    if text is None:
        text = ''
    text = text.strip()
    return text


SENTENCE_ENDERS = re.compile('[.!?]')
def extract_sentences(cleaned_text):
    return [s.strip() for s in SENTENCE_ENDERS.split(cleaned_text) if s]


def parse_sentence(sentence):
    return [Word(word) for word in sentence.split(' ')]


class SentenceScanner(object):
    def __init__(self, sentence):
        self.sentence = sentence
        self.compounds = []
        self.immediate_compound = None

    def scan(self):
        for i in range(1, len(self.sentence)):
            current_word, previous_word = self.sentence[i], self.sentence[i - 1]

            print "prev: %s\ncurrent: %s\n" % (previous_word, current_word)

            # Special case
            if is_a_dish_name([previous_word, current_word]):
                self._add_compound(4, previous_word, current_word)
                continue

            if previous_word.is_noun and current_word.is_noun:
                if current_word.is_third_person_possive and previous_word.is_bare:
                    self._add_compound(1, previous_word, current_word)
                elif self.immediate_compound:
                    words = self.immediate_compound.words + [current_word]
                    self._add_compound(1, *words)
                else:
                    self.immediate_compound = None
            else:
                self.immediate_compound = None

            print "----"

        return self.compounds

    def _add_compound(self, type, *words):
        compound = NounCompound(list(words), type)
        self.compounds.append(compound)
        self.immediate_compound = compound


def main():
    sentences = ["kırmızı kedi yürüyor",
        "kadın avukat çok güzel",
        "sarı humma",
        "çay bahçesi",
        "otobüs biletlerinden",
        "otobüs biletleri çok pahalı",
        "el çantaları",
        "çay bardakları",
        "Ağrı Dağı"
        ]
    for sentence in sentences:
        parsed_sentence = parse_sentence(sentence)
        for compound in find_noun_compounds(parsed_sentence):
            print compound


if __name__ == '__main__':
    main()


text = """

masa örtüsü. çay bardağı. çocuk bahçesi. otobüs biletleri. el çantaları.
çay bardakları. ayakkabılar. yüzbaşılar. buzdolapları. Ağrı Dağı. Van
Gölü. Efes Lokantası. Türkiye Cumhuriyeti. İngiliz Edebiyatı Bölümü. göz hastalıkları hastanesi.
Türkiye Cumhuriyeti Milli Eğitim Bakanlığı Kadıköy Kız Yüksek Meslek Okulu.
"""
