#coding: utf-8

tests = (
    ("cccvcc", 5),
    ("cccvcv", 4),
    ("cccvc", 5),
    ("ccvccc", 5),
    ("ccvccv", 4),
    ("ccvcc", 5),
    ("ccvcc", 4),
    ("ccvcv", 3),
    ("ccvc", 4),
    ("ccvv", 3),
    ("ccv", 3),
    ("cvccc", 4),
    ("cvccv", 3),
    ("cvcc", 4),
    ("cvcv", 2),
    ("cvc", 3),
    ("cv", 2),
    ("vccc", 3),
    ("vccv", 2),
    ("vcc", 3),
    ("vcvc", 1),
    ("vcv", 1),
    ("vc", 2),
    ("v", 1),
)

vowels = set([u'a', u'A', u'e', u'E', u'ı', u'I', u'i', u'İ',
              u'o', u'O', u'ö', u'Ö', u'u', u'U', u'ü', u'Ü'])


def is_vowel(letter):
    return letter in vowels


def convert_to_cv(word):
    return u''.join(is_vowel(letter) and 'v' or 'c' for letter in word)


def syllables(word):
    original_word = word
    cv_form = convert_to_cv(word)
    syllables = []

    while cv_form:
        found = False
        for test, length in tests:
            if cv_form.startswith(test):
                syllables.append(word[:length])
                cv_form = cv_form[length:]
                word = word[length:]
                found = True
                break

        if not found:
            raise Exception('cannot syllabilize %s', original_word)
            break

    return syllables


def main():
    print syllables(u'haldun')
    print syllables(u'çekoslavakya')
    print syllables(u'jhjjkjkjkj')

if __name__ == '__main__':
    main()
