# Python imports
import os

# Tornado imports
import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options
from tornado.web import url

import compounds

define("port", default=8888, help="run on the given port", type=int)
define("debug", default=False, type=bool)

PROJECT_ROOT = os.path.dirname(__file__)

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            url(r'/', MainHandler, name='main'),
        ]
        settings = {
            'debug': options.debug,
            'static_path': os.path.join(PROJECT_ROOT, 'static'),
            'template_path': os.path.join(PROJECT_ROOT, 'templates'),
            'xsrf_cookies': True,
            'cookie_secret': 'iduh892983u129ubasdh0123-lsadpi0123asd979y1232',
        }
        tornado.web.Application.__init__(self, handlers, **settings)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('form.html')

    def post(self):
        text = self.get_argument('text', '')
        if not text:
            self.redirect('/')
            return
        text = text.strip().encode('utf-8')
        self.render('results.html', text=text, result=compounds.sentence_to_sexp(compounds.process_sentence(text)))

def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()

