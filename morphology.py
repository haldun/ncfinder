# coding: utf-8

import operator
import re

import TurkishMorphology
import syllables

class Word(object):
    TurkishMorphology.load_lexicon('turkish.fst')

    def __init__(self, original):
        self.original = original
        self.root = None
        self.attrs = []
        self.is_parsed = False
        self.attrs_set = set()
        self._parse()

    def __repr__(self):
        return "<Word: '%s', root: '%s', attrs: %s>" % (self.original, self.root, self.attrs)

    def __str__(self):
        return self.original

    @property
    def has_suffix(self):
        return self.original != self.root

    @property
    def is_bare(self):
        return not self.has_suffix

    @property
    def is_profession(self):
        return self.root in set(['doktor', 'avukat'])

    @property
    def is_gender(self):
        return self.root in set(['kadın', 'erkek', 'kız'])

    @property
    def is_nationality(self):
        return self.root in set(['Türk', 'Alman'])

    @property
    def is_noun(self):
        return 'Noun' in self.attrs

    @property
    def is_adjective(self):
        return 'Adj' in self.attrs

    @property
    def is_third_person_possive(self):
        return bool(set(['P3sg', 'P3pl']) & self.attrs_set)

    @property
    def is_nominative(self):
        return 'Nom' in self.attrs_set

    @property
    def is_plural(self):
        for attr in self.attrs_set:
            if 'pl' in attr:
                return True
        return False

    @property
    def is_material(self):
        # TODO Look-up from a list of words
        return self.root in set(['çelik', 'taş', 'yün', 'Çelik'])

    @property
    def is_street(self):
        return self.root == 'sokak'

    @property
    def is_compound(self):
        if not hasattr(self, '_is_compound'):
            for i in range(self.syllables):
                print i
        return self._is_compound

    @property
    def subwords(self):
        if not hasattr(self, '_subwords'):
            self._subwords = self._parse_subwords()
        return self._subwords

    @property
    def syllables(self):
        if not hasattr(self, '_syllables'):
            self._syllables = syllables.syllables(unicode(self.original, 'utf8'))
        return self._syllables

    def _parse(self):
        parses = sorted(TurkishMorphology.parse(self.original), key=operator.itemgetter(1))

        if not parses:
            return

        best_parse = parses[0][0]
        self.root = best_parse[:best_parse.find('[')]

        for match in re.findall("(?P<attr>\[\w+\])", best_parse):
            self.attrs.append(match[1:-1])

        self.attrs_set = set(self.attrs)
        self.is_parsed = True

    def _parse_subwords(self):
        pass


def main():
    w = Word('ışık')
    for s in w.syllables:
        print s

if __name__ == '__main__':
    main()
